//banner数据
const banner = [{
    id: 1,
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
}, {
    id: 2,
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
}, {
    id: 3,
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
}]
//菜单数据
const home_menu = [{
    id: 1,
    img: '../../../resource/推荐客户.png',
    title: '推荐客户'
}, {
    id: 2,
    img: '../../../resource/我的佣金.png',
    title: '我的佣金'
}, {
    id: 3,
    img: '../../../resource/看房团登记.png',
    title: '我的预约'
}]
//新闻数据
const news = [{

}]
//楼盘数据
const houses = [{
        id: 1,
        img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
        name: '恒大金碧城',
        price: '40000', //价格40000元/m
        addr: {
            area: '荔湾',
            village: '茶胶',
            acreage: '建面79-146m',
        },
        titles: ['免费专车', '品牌房企', '小户型']
    },
    {
        id: 1,
        img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
        name: '恒大金碧城',
        price: '40000', //价格40000元/m
        addr: {
            area: '荔湾',
            village: '茶胶',
            acreage: '建面79-146m',
        },
        titles: ['免费专车', '品牌房企', '小户型']
    }
]

//文章数据
const articles = [{
    id: 1,
    title: '二手房及租赁价格指数下跌，四线城市环比降幅明显',
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userImg: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userName: '恒大地产',
    read: 1234
}, {
    id: 2,
    title: '二手房及租赁价格指数下跌，四线城市环比降幅明显',
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userImg: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userName: '恒大地产',
    read: 1234
}, {
    id: 3,
    title: '二手房及租赁价格指数下跌，四线城市环比降幅明显',
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userImg: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userName: '恒大地产',
    read: 1234
}, {
    id: 4,
    title: '二手房及租赁价格指数下跌，四线城市环比降幅明显',
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userImg: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userName: '恒大地产',
    read: 1234
}, {
    id: 5,
    title: '二手房及租赁价格指数下跌，四线城市环比降幅明显',
    img: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userImg: 'http://t2.hddhhn.com/uploads/tu/201610/198/scx30045vxd.jpg',
    userName: '恒大地产',
    read: 1234
}]


module.exports = {
    banner: banner,
    home_menu: home_menu,
    news: news,
    houses: houses,
    articles: articles
}